export type TextInputElement = HTMLInputElement | HTMLTextAreaElement
export function isTextInputElement(element: Element): element is TextInputElement {
    element.nodeType
    return element.tagName == "TEXTAREA"
        || (element.tagName == "INPUT" && (element as HTMLInputElement).type == "text")
}