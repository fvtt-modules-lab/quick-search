import { SearchItem, SearchResult } from './searchLib'
import { TextInputElement } from './utils'

export interface SearchFilterItem {
    tag: string
    sub: string
    indexes: string[]
    postFilter?: (item: SearchResult) => boolean
}

export class SearchContext {
    spawnCSS = {}
    filter?: string | SearchFilterItem
    onSubmit(item: SearchItem): void {
        // Render the sheet for selected item
        item.show()
    }
    onClose(): void { return }
}

export class InputContext extends SearchContext {
    input: TextInputElement
    constructor(input: TextInputElement) {
        super()
        this.input = input
        const targetRect = input.getBoundingClientRect()
        const bodyRect = document.body.getBoundingClientRect()
        const top = targetRect.top - bodyRect.top
        // TODO: Real calculation!!!
        this.spawnCSS = {
            "left": targetRect.left + 5,
            "bottom": bodyRect.height - top - 25,
            "width": targetRect.width - 10,
        }

        $(input).addClass("quick-search-context")
    }
    onSubmit(item: SearchItem): void {
        this.input.value = this.input.value + item.journalLink
    }
    onClose(): void {
        $(this.input).removeClass("quick-search-context")
        this.input.focus()
    }
}

export class ScriptMacroContext extends InputContext {
    onSubmit(item: SearchItem): void {
        this.input.value = this.input.value + item.script
    }
}

export class RollTableContext extends InputContext {
    constructor(input: TextInputElement) {
        super(input)
        // Set filter depending on selected dropdown!
        // const resultRow = this.input.closest("li.table-result")
    }
    onSubmit(item: SearchItem): boolean {
        // TODO: handle duplicate names?
        // Entities input can take id, but compendium doesn't anymore
        this.input.value = item.name
        return false
    }
}

export class TinyMCEContext extends SearchContext {
    editor: TinyMce
    constructor(editor: TinyMce) {
        super()
        this.editor = editor
    }
    onSubmit(item: SearchItem): boolean {
        this.editor.insertContent(item.journalLink)
        return false
    }
}