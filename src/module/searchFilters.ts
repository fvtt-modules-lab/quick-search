import { SearchResult } from './searchLib'

export interface SearchFilterItem {
    tag: string
    sub: string
    indexes: string[]
    postFilter?: (item: SearchResult) => boolean
}

export const defaultFilters: SearchFilterItem[] = [
    {
        tag: "actor",
        sub: "Actors Items Directory",
        indexes: ["actors"],
    },
    {
        tag: "item",
        sub: "Items Directory",
        indexes: ["items"],
    },
    {
        tag: "journal",
        sub: "Journals Directory",
        indexes: ["journals"],
    },
    // {
    //     tag: "rolltable",
    //     sub: "RollTable Directory",
    //     indexes: ["rolltables"],
    // },
    // {
    //     tag: "scene",
    //     sub: "Scene Directory",
    //     indexes: ["scenes"],
    // },
]