import Fuse from 'fuse.js'

const entityIcons = {
    "Actor": "fa-user",
    "Item": "fa-suitcase",
    "JournalEntry": "fa-book-open",
    "Macro": "",
    "Playlist": "fa-music",
    "RollTable": "fa-th-list",
    "Scene": "fa-map",
    "User": "fa-users",
}

const entityCollections = {
    "Actor": "actors",
    "Item": "items",
    "JournalEntry": "journal",
    "Macro": "macros",
    "Playlist": "playlists",
    "RollTable": "tables",
    "Scene": "scenes",
    "User": "users",
}

interface CompendiumIndexItem {
    id: string  // pre-5.5 compat
    _id: string
    img: string
    name: string
}

export class SearchItem {
    name: string
    id: string
    img: string

    constructor(id: string, name: string, img?: string) {
        this.id = id
        this.name = name
        this.img = img
    }

    // Get the uuid value compatible with fromUuid
    get uuid(): string { return "" }
    // Get the draggable attributes in order to make custom elements
    get draggableAttrs(): string { return "" }
    // Get a clickable (preferrably draggable) link to the entity
    get link(): string { return "" }
    // Reference the entity in a journal, chat or other places that support it
    get journalLink(): string { return "" }
    // Reference the entity in a script
    get script(): string { return "" }
    // Short tagline that explains where/what this is
    get tagline(): string { return "" }
    // Show the sheet or equivalent of this search result
    show(): void { return }
}

export interface SearchResult {
    item: SearchItem
    score: number
    refIndex: number
}

export class EntitySearchItem extends SearchItem {
    entityType: | "Actor" | "Item" | "JournalEntry" | "Macro"
        | "Playlist" | "RollTable" | "Scene" | "User"

    folder?: { id: string, name: string }

    // Get the draggable attributes in order to make custom elements
    get draggableAttrs(): string {
        return `draggable="true" data-entity="${this.entityType}" data-id="${this.id}"`
    }

    // Get a draggable and clickable link to the entity
    get link(): string {
        return `<a class="entity-link" ${this.draggableAttrs}><i class="fas ${entityIcons[this.entityType]}"></i> ${this.name}</a>`
    }

    // Reference the entity in a journal, chat or other places that support it
    get journalLink(): string {
        return `@${this.entityType}[${this.id}]{${this.name}}`
    }

    // Reference the entity in a script
    get script(): string {
        return `game.${entityCollections[this.entityType]}.get("${this.id}")`
    }

    // Short tagline that explains where/what this is
    get tagline(): string {
        return `${this.entityType}`
    }

    show(): void {
        game[entityCollections[this.entityType]].get(this.id).sheet.render(true)
    }

    constructor(entity: Entity) {
        super(entity.id, entity.name, entity.data["img"])

        const folder = entity.folder
        if (folder) {
            this.folder = {
                id: folder.id,
                name: folder.name
            }
        }

        // @ts-ignore // entity.entity is a string getter
        this.entityType = entity.entity
    }
}

export class CompendiumSearchItem extends SearchItem {
    label: string
    package: string
    entityType: | "Actor" | "Item" | "JournalEntry" | "Macro"
        | "Playlist" | "RollTable" | "Scene" | "User"

    constructor(pack: Compendium, item: CompendiumIndexItem) {
        super(item.id || item._id, item.name, item.img)
        this.package = pack.collection
        this.entityType = pack.entity as any
    }

    get uuid(): string {
        return `Compendium.${this.package}.${this.id}`
    }

    // Get the draggable attributes in order to make custom elements
    get draggableAttrs(): string {
        // data-id is kept for pre-5.5 compat
        return `draggable="true" data-pack="${this.package}" data-id="${this.id}" data-lookup="${this.id}"`
    }

    // Get a draggable and clickable link to the entity
    get link(): string {
        return `<a class="entity-link" ${this.draggableAttrs}><i class="fas ${entityIcons[this.entityType]}"></i> ${this.name}</a>`
    }

    // Reference the entity in a journal, chat or other places that support it
    get journalLink(): string {
        return `@Compendium[${this.package}.${this.id}]{${this.name}}`
    }

    // Reference the entity in a script
    get script(): string {
        return `fromUuid("${this.uuid}")` // TODO: note that this is async somehow?
    }

    // Short tagline that explains where/what this is
    get tagline(): string {
        return `${this.package}`
    }

    show(): void {
        fromUuid(this.uuid).then((entity: Entity) => entity.sheet.render(true))
    }
}

export interface IndexOptions {
    actors?: true
    items?: true
    journal?: true
    macros?: true
    playlists?: true
    tables?: true
    scenes?: true
    compendium?: true
}

export interface SearchList {
    items: SearchItem[]
    keys: string[] // Indexed keys
}

class EntityIndex implements SearchList {
    items: EntitySearchItem[]
    keys: string[] = ["name"]
    constructor(entities: Entity[]) {
        this.items = entities
            .filter(e => e.visible)
            .map(entity => new EntitySearchItem(entity))
    }
}

class CompendiumIndex implements SearchList {
    items: CompendiumSearchItem[]
    keys: string[] = ["name"]

    static async build(compendium: Compendium): Promise<CompendiumIndex> {
        const cIndex = await compendium.getIndex()
        const index = new CompendiumIndex()
        index.items = cIndex.map((item: CompendiumIndexItem) => new CompendiumSearchItem(compendium, item))
        return index
    }
}

export class SearchLib {
    indexes: { [key: string]: Fuse<SearchItem, any> } = {}
    folders: Entity[]

    addIndex(indexName: string, list: SearchList): void {
        this.indexes[indexName] = new Fuse(list.items, {
            includeScore: true,
            keys: list.keys,
            // includeMatches: true, // uncomment if we end up using it
            options: {
                shouldSort: false, // Will sort on index merge
                threshold: 0.3,
            }
        })
    }
    async indexCompendiums(): Promise<void> {
        await Promise.all(game.packs.map(async pack => {
            const index = await CompendiumIndex.build(pack)
            this.addIndex(pack.collection, index)
        }))
    }

    indexDefaults(options: IndexOptions = { actors: true, items: true, journal: true }): void {
        if (options.actors) { this.addIndex("actors", new EntityIndex(game.actors.entities)) }
        if (options.items) { this.addIndex("items", new EntityIndex(game.items.entities)) }
        if (options.journal) { this.addIndex("journals", new EntityIndex(game.journal.entities)) }
    }

    searchIndexes(text: string, indexes: Fuse<SearchItem, any>[], max?: number): Fuse.FuseResult<SearchItem>[] {
        if (max !== undefined) {
            const results = indexes.map(i => i.search(text, { limit: max }))
            // This may look unsophisticated, but it's a fraction of the time used for searching
            const joined = ([] as Fuse.FuseResult<SearchItem>[]).concat(...results)
            return joined.sort((a, b) => a.score - b.score).slice(0, max)
        }

        const results = indexes.map(i => i.search(text))
        const joined = ([] as Fuse.FuseResult<SearchItem>[]).concat(...results)
        return joined.sort((a, b) => a.score - b.score)
    }

    search(text: string, indexes?: string[] | false, max?: number): Fuse.FuseResult<SearchItem>[] {
        const selectedIndexes = indexes
            ? indexes.map(idx => this.indexes[idx])
            : Object.values(this.indexes)

        return this.searchIndexes(text, selectedIndexes, max)
    }
}
