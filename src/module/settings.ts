const MODULE_NAME = "quick-search"

export const settings = {
    QUICKOPEN: "quickOpen",
    SEARCH_ANYWHERE_COMPAT: "searchAnywhereCompatible"
}

export function registerSettings(): void {
    game.settings.register(MODULE_NAME, settings.QUICKOPEN, {
        name: 'Open QuickSearch',
        hint: 'Enter a key combination that opens QuickSearch in the active context',
        type: window.Azzu.SettingsTypes.KeyBinding,
        default: 'Ctrl +  ',
        scope: 'client',
        config: true
    })

    // TODO: Figure out what we need to do for this
    // game.settings.register(MODULE_NAME, settings.SEARCH_ANYWHERE_COMPAT, {
    //     name: 'Search Anywhere Fallback',
    //     hint: "Fallback to Search Anywhere if QuickSearch can't infer a context",
    //     // @ts-ignore
    //     type: Boolean,
    //     default: false,
    //     scope: 'client',
    //     config: true
    // })
}