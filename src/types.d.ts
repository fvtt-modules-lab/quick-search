// Extra types not covered by foundry-pc-types
interface Window {
    Azzu: any
}

// TODO: maybe include tinymce types via npm
interface TinyMce {
    insertContent(string)
    PluginManager: {
        add(arg0: string, arg1: Function)
    }
}
declare let tinymce: TinyMce

declare let fromUuid: (uuid: string) => Promise<Entity | null>
