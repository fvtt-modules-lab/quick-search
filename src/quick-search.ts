import { SearchLib, SearchItem } from './module/searchLib'
import { registerSettings, settings } from './module/settings'
import { isTextInputElement } from './module/utils'
import { SearchFilterItem, defaultFilters } from './module/searchFilters'

import { InputContext, ScriptMacroContext, TinyMCEContext, RollTableContext } from "./module/contexts"

const MODULE_NAME = "quick-search"

// Some black magic from the internet,
// places caret at end of contenteditable
function placeCaretAtEnd(el: HTMLElement): void {
    el.focus()
    const range = document.createRange()
    range.selectNodeContents(el)
    range.collapse(false)
    const sel = window.getSelection()
    sel.removeAllRanges()
    sel.addRange(range)
}

class SearchContext {
    spawnCSS = {}
    filter?: string | SearchFilterItem
    onSubmit(item: SearchItem): void {
        // Render the sheet for selected item
        item.show()
    }
    onClose(): void { return }
}

abstract class SearchMode {
    app: QuickSearch
    constructor(app: QuickSearch) {
        this.app = app
    }

    abstract onTab(index: number): void
    abstract onEnter(index: number): void
    abstract search(textInput: string): string
}

interface SearchResult {
    item: SearchItem
    score: number
    refIndex: number
}

class SearchEntities extends SearchMode {
    results: SearchResult[]
    onTab(index: number): void {
        void (index)
        return
        // TODO: What is appropriate?
    }

    onEnter(index: number): void {
        this.app.activeContext.onSubmit(this.results[index].item)
        this.app.closeDialog()
    }

    search = (textInput: string): string => {
        textInput = textInput.trim()
        if (textInput.length == 0) { return "" }
        // Set a lower maximum if search is single char (single-character search is fast, but rendering is slow).
        const max = textInput.length == 1 ? 20 : 100
        if (this.app.selectedFilter) {
            this.results = ui.searchLib.search(textInput, this.app.selectedFilter.indexes, max)
        } else {
            this.results = ui.searchLib.search(textInput, undefined, max)
        }
        // TODO: don't use res.item.link. Make entire li the "link"
        return this.results.map((res: SearchResult) => `<li><span class="title">${res.item.link}</span><span class="sub">${res.item.tagline}</span></li>`).join("")
    }
}

class SearchFilters extends SearchMode {
    filteredFilters: SearchFilterItem[] = []
    filters: SearchFilterItem[] = defaultFilters

    dynamicCompendiumFilters(): void {
        this.filters = this.filters.concat(game.packs.map((pack: Compendium) => {
            return {
                tag: pack.collection,
                sub: pack.metadata.label,
                indexes: [pack.collection],
            }
        }))
    }

    onTab(index: number): void {
        this.onEnter(index)
    }

    onEnter(index: number): void {
        this.selectFilter(this.filteredFilters[index])
    }

    selectFilter(filter: SearchFilterItem): void {
        this.app.setFilterTag(filter)
        this.app.selectedFilter = filter
        this.app.output.html(`<li>Searching: ${filter.sub}</li>`)
    }

    search(textInput: string): string {
        const cleanedInput = textInput.toLowerCase().trim()
        if (/\s$/g.test(textInput)) {
            // User has added a space after tag -> selected
            const matchingFilter = this.filters.find(f => f.tag == cleanedInput)
            if (matchingFilter) {
                this.selectFilter(matchingFilter)
                return
            }
        }
        this.filteredFilters = this.filters.filter(f => f.tag.startsWith(cleanedInput))
        return this.filteredFilters.map(res => `<li><span class="title">@${res.tag}</span><span class="sub">${res.sub}</span></li>`).join("")
    }
}

enum ActiveMode {
    Search = 1,
    Filter,
}

class QuickSearch extends Application {
    _state: number
    mouseFocus = false
    inputFocus = false
    mode: ActiveMode = ActiveMode.Search
    selectedFilter: SearchFilterItem = null
    activeContext: SearchContext
    selectedIndex = -1

    searchFilters = new SearchFilters(this)
    searchEntities = new SearchEntities(this)

    input: JQuery
    output: JQuery

    public get searchMode(): SearchMode {
        if (this.mode === ActiveMode.Filter) {
            return this.searchFilters
        } else if (this.mode === ActiveMode.Search) {
            return this.searchEntities
        }
    }

    constructor() {
        super({
            template: "modules/quick-search/templates/quick-search.html",
            popOut: false,
        })
    }

    selectNext(): void {
        this.selectResult((this.selectedIndex + 1) % this.output.children().length)
    }

    selectPrevious(): void {
        this.selectResult(this.selectedIndex > 0 ? this.selectedIndex - 1 : this.output.children().length - 1)
    }

    selectResult(resultIndex): void {
        if (this.output.children().length === 0) {
            return
        }
        this.selectedIndex = resultIndex
        this.output.find(".search-selected").removeClass("search-selected")

        const selected = $(this.output.children()[resultIndex])
        selected.addClass("search-selected")

        const list = $(this.element).find(".quick-search-result-wrapper")
        if (selected.offset().top < 1) {
            // Selected is above fold
            list.scrollTop(selected.offset().top - list.offset().top + list.scrollTop())
        }
        if (selected.offset().top + selected.height() > list.height()) {
            // Selected is below fold
            list.scrollTop(selected.offset().top - list.offset().top + list.scrollTop() - list.height() + selected.outerHeight()) // This scrolls to the right one
        }
    }

    setFilterTag(filter: SearchFilterItem): void {
        this.input.html("")
        $(`<span class="search-tag">@${filter.tag}</span>`).prependTo(this.input)
        $('<span class="breaker">&nbsp</span>').appendTo(this.input)
        placeCaretAtEnd(this.input.get(0))
    }

    closeDialog(): void {
        this.activeContext?.onClose()
        this.close()
    }

    render(force: boolean, options: { context: SearchContext }): Application {
        if (this._state > 0) { return null }

        if (options && options.context) {
            this.activeContext = options.context
            return super.render(force, options)
        }

        // Try to infer context
        const target = document.activeElement
        if (target && isTextInputElement(target)) {
            if (target.name === "command" && (target.closest(".macro-sheet").querySelector('select[name="type"]') as HTMLSelectElement).value === "script") {
                this.activeContext = new ScriptMacroContext(target)
            } else if (target.name.startsWith("results.") && target.closest(".result-details")) {
                this.activeContext = new RollTableContext(target)
            } else {
                this.activeContext = new InputContext(target)
            }
        } else {
            // No/unknown context, simply open the selected item on submit
            console.log("unknown context")
            this.activeContext = new SearchContext()
        }

        return super.render(force, options)
    }

    showResults(searchContents: string): void {
        this.output.html(searchContents)
        this.selectResult(0)
    }

    activateListeners(html: JQuery): void {
        // (Re-)set position
        $(html).removeAttr("style")
        if (this.activeContext.spawnCSS) {
            Object.keys(this.activeContext.spawnCSS).forEach(key => {
                $(html).css(key, this.activeContext.spawnCSS[key])
            })
        }
        this.input = html.find(".search-editable-input")
        this.output = html.find(".quick-search-result")

        this.input.on("input", (evt: JQuery.TriggeredEvent<HTMLElement>) => {

            const text: string = evt.target.innerText
            const breaker = $(evt.target).find(".breaker")

            if (this.selectedFilter) {
                // Text was changed or breaker was removed
                if (!text.startsWith(`@${this.selectedFilter.tag}`)
                    || breaker.length === 0 || breaker.is(":empty")) {
                    // Selectedfilter doesn't match any more :(
                    this.input.html(text)
                    placeCaretAtEnd(this.input.get(0))
                    this.selectedFilter = null
                    this.mode = ActiveMode.Filter
                    this.showResults(this.searchFilters.search(text.substr(1).trim()))
                } else {
                    this.mode = ActiveMode.Search
                    const search = text.replace(`@${this.selectedFilter.tag}`, "").trim()
                    this.showResults(this.searchEntities.search(search))
                }
            } else if (text.startsWith("@")) {
                this.mode = ActiveMode.Filter
                this.showResults(this.searchFilters.search(text.substr(1)))
            } else {
                this.mode = ActiveMode.Search
                this.showResults(this.searchEntities.search(text))
            }
        })

        const keyCodeBinds = {
            13: this._onKeyEnter,
            40: this._onKeyDown,
            38: this._onKeyUp,
            27: this._onKeyEsc,
            9: this._onKeyTab,
        }

        this.input.on("keydown", evt => {
            if (keyCodeBinds[evt.which]) {
                evt.preventDefault()
                keyCodeBinds[evt.which]()
            }
        })

        $(this.element).hover(() => {
            this.mouseFocus = true
            this._checkFocus()
        }, () => {
            this.mouseFocus = false
            this._checkFocus()
        })

        $(this.element).on("focusout", () => {
            this.inputFocus = false
            this._checkFocus()
        })

        this.inputFocus = true
        this.input.focus()
    }

    _checkFocus = (): void => {
        if (!this.mouseFocus && !this.inputFocus) {
            this.closeDialog()
        }
    }
    _onKeyTab = (): void => this.searchMode.onTab(this.selectedIndex)
    _onKeyEsc = (): void => this.closeDialog()
    _onKeyDown = (): void => this.selectPrevious()
    _onKeyUp = (): void => this.selectNext()
    _onKeyEnter = (): void => this.searchMode.onEnter(this.selectedIndex)
}

Hooks.once('init', async function () {
    // TinyMCE addon registration
    tinymce.PluginManager.add('quicksearch', function (editor) {
        editor.ui.registry.addButton('quicksearch', {
            text: 'QuickSearch',
            icon: 'search',
            onAction: function () {
                // Open window
                ui.quickSearch.render(true, {
                    context: new TinyMCEContext(editor)
                })
            }
        })
    })
    CONFIG.TinyMCE.plugins = CONFIG.TinyMCE.plugins + " quicksearch"
    CONFIG.TinyMCE.toolbar = CONFIG.TinyMCE.toolbar + " quicksearch"
    registerSettings()
})

Hooks.once('ready', async function () {
    const searchLib = new SearchLib()

    searchLib.indexDefaults()
    await searchLib.indexCompendiums()
    ui.searchLib = searchLib
    console.log('QuickSearch | Search index available')

    ui.quickSearch = new QuickSearch()
    ui.quickSearch.searchFilters.dynamicCompendiumFilters()

    $(document).on("keydown", evt => {
        const key = window.Azzu.SettingsTypes.KeyBinding.parse(game.settings.get(MODULE_NAME, settings.QUICKOPEN))
        if (window.Azzu.SettingsTypes.KeyBinding.eventIsForBinding(evt.originalEvent, key)) {
            evt.stopPropagation()
            ui.quickSearch.render(true)
        }
    })

    Hooks.callAll("QuickSearch:Loaded")
    console.log('QuickSearch | Quick Search ready')
})
