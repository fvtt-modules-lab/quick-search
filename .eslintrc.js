module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    plugins: [
        '@typescript-eslint',
    ],
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
    ],
    rules: {
        "semi": ["error", "never"],
        "comma-dangle": ["error", {
            "arrays": "always-multiline",
            "objects": "only-multiline",
            "imports": "only-multiline",
            "exports": "only-multiline",
            "functions": "only-multiline",
        }],
        "@typescript-eslint/ban-ts-ignore": ["warn"],
        "@typescript-eslint/member-delimiter-style": ["warn", {
            "multiline": {
                "delimiter": "none",
                "requireLast": false
            },
            "singleline": {
                "delimiter": "comma",
                "requireLast": false
            }
        }]
    }
};