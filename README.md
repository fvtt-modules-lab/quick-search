# QuickSearch - Find and insert entities you need
QuickSearch provides a context-aware search tool that can be used in most parts of FoundryVTT.
Quickly search, view, or insert found entities.

Where integrated, QuickSearch can automatically insert the results in the active context (select with arrow keys and `enter`). For most other things, there's drag-drop, as provided by the FoundryVTT core.

## Background
QuickSearch is inspired by smarter search interfaces, such as search engines, command pallettes, browser address bars, autocomplete/intellisense, as well as the [Search Anywhere](https://gitlab.com/riccisi/foundryvtt-search-anywhere/) FoundryVTT module.

## Installation
QuickSearch in a pre-release state and is not available in the module browser. It can be installed manually with the following manifest URL
```
https://gitlab.com/fvtt-modules-lab/quick-search/-/jobs/artifacts/master/raw/module.json?job=build-module
```

## Usage
Open the QuickSearch window with the keyboard shortcut `Ctrl + Space` (editable in settings), or with the QuickSearch button in rich-text editors.

Most search results can be interacted with in multiple ways:
- Mouse click -> **inspect** or **show**
- Select with arrow keys and `enter` -> **submit**
    - If attached to a context (like an input field - generally marks the input with a red border), `enter` automatically inserts a reference
    - Otherwise, the same as mouse click
    - There will be better visual hints for this difference in the future
- Drag-drop directly from the search results
- **Use search filters:** start by typing `@` and select filter with `tab` or `enter`
    - This will be selectable by click as well


## Search sources
Quick Search is built with the intention of supporting many searchable sources.
For now, these are:
- Entity directories
    - Actors
    - Items
    - Journals
- Compendiums

## Integrated search context
QuickSearch attempts to attach itself to a certain **"context"** for automatic insertion. As of now, these are:
- Journals and other TinyMCE(rich text) editors, via QuickSearch button in the tool bar
- Macro editor (both script and chat macros)
- Chat (Note: @ tags may conflict with mentions if it's in the start of the message)
- Rollable table editor (enhanced integration coming soon)
- Modules may spawn a QuickSearch prompt with a custom context (PM me on Discord if you want to integrate)

## Known issues (also see [TODO.md](./TODO.md))
- The search index is built on page load, if something is missing - refresh the page

### Performance in firefox
Search performance in Firefox degrades on longer search inputs. This is an issue with Firefox and the fuzzy search library used by QuickSearch - [Fuse.js](https://fusejs.io/).