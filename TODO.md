# Required for first release
Tagged with **A**
# Need to have
- [x] Search input is contenteditable
- [ ] @ to select collection
    - [x] search and select from search result
    - [x] adding space after a valid @ sets the filter tag
    - [ ] select with click **A**
- [x] Search filters automatically generated for all found packages
- [x] Better close behaviour for mouse interaction with result list
- [x] Compendium search
    - [x] Index compendium
    - [x] Compendium tagline
- [ ] Add better taglines for all items
    - [ ] Compendium source, improve by using nice name
    - [ ] Entity folder
    - [ ] System-specific subtype
    - [ ] Set title for each entry to `name - tagline`
        - Important to be able to access full info for narrow search output
- [ ] Add settings
    - [ ] Select collections to index
    - [ ] Automatically find all possible collections to index
    - [ ] Allow other modules to add indexes?
- [ ] Ensure compendium compatibility with top systems
    - [ ] Minimal compatibility with ALL systems (if possible)
    - [ ] Test with 3-popular systems

- [ ] Re-indexing hooks
    - Investigate Fuse.js next(6.0)
    - [ ] Full re-index **A**
    - [ ] Per-collection re-index

# Nice to have
- [ ] Search filters pre-applied by context
- Search buttons in selected applications
    - [x] Journal editor (no filter affinity)
    - [ ] Character sheet search
        - [ ] 5e items
        - [ ] 5e spells
        - [ ] 5e class features
        - [ ] 5e classes
- [ ] Fuzzy search in filters
- [ ] Post-filter
    - (custom) filter, supply a function that filters the results by entity data
- [ ] Icons and meta
    - [ ] Show entity images when found (addition  to type icon?)
    - [ ] Show subtype icons? `[img] Name (spacing) (?tagline) {?subtype icons} {source icon}`
    - [ ] Better taglines (system data)
- [ ] Don't attach to uninteresting input contexts
    - [ ] All(?) inputs in character sheets
    - [ ] Search boxes (usually name="search)
    - [ ] App blacklist?

- [ ] Manual rendering? (instead of using core Application class)
    - [ ] Keep last search?

- [ ] Hint/placeholder text in input (depending on filter and/or context)
- [ ] Investigate whether we can use keyboard shortcut in
- [ ] Hold `shift` (or other modifier) when pressing `enter` to submit without closing search window
- [ ] Window focus: identify re-focus of input field (never close if input is actually focused)
- [ ] Throttle search. Especially needed on long input on firefox (input can be faster than search, causing significant lag)
